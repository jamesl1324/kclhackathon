from tkinter import *
# - = air
# # = block
map = [['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
       ['#', '#', '#', '#', '#', '-', '#', '#', '#', '#'],
       ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
       ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
       ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#']]

simplemap = [['#', '#', '#', '#']]

scale = 100
size = 500
circlescale = 20

root = Tk()
can = Canvas(root, height = size, width = size)
can = Canvas(root, height = len(map) * scale, width = len(map[0]) * scale)
can.pack()

def canprint(map):
    can.create_oval(0, 0, size, size)
    for y in range(len(map)):
        y += 1
        y *= circlescale
        can.create_oval(y, y, size - y, size - y)
    """for y in range(len(map)):
        for x in range(len(map[0])):
            if map[y][x] == '-':
                can.create_rectangle(x*scale, y*scale, x*scale+scale, y*scale+scale, fill = 'BLUE')
            if map[y][x] == '#':
                can.create_rectangle(x*scale, y*scale, x*scale+scale, y*scale+scale, fill = 'RED')
                #can.create_rectangle(, 10, 20, 20, fill = 'RED')"""
canprint(map)

root.mainloop()