from tkinter import *
from tkinter import ttk
import maputil
import Collision

# - = air
# # = block
map = maputil.getmap()

player = [2, 5]
playerhw = [20, 10]

simplemap = [['#', '#', '#', '#']]

scale = 15
size = 500
circlescale = 20

root = Tk()
root.title('Platformer')
can = Canvas(root, height = len(map) * scale, width = len(map[0]) * scale)
can.pack()

def canprint(map, player, playerhw):
    can.delete(ALL)
    for y in range(len(map)):
        for x in range(len(map[0])):
            if map[y][x] == '-':
                can.create_rectangle(x*scale, y*scale, x*scale+scale, y*scale+scale, fill = 'WHITE')
            if map[y][x] == '#':
                can.create_rectangle(x*scale, y*scale, x*scale+scale, y*scale+scale, fill = 'RED')
            if map[y][x] == 'A':
                can.create_rectangle(x*scale, y*scale, x*scale+scale, y*scale+scale, fill = 'GREEN')
            if map[y][x] == 'S':
                can.create_rectangle(x*scale, y*scale, x*scale+scale, y*scale+scale, fill = 'YELLOW')
                #can.create_rectangle(, 10, 20, 20, fill = 'RED')
    print(player[0] * scale, player[1] * scale, player[0] * scale + scale, player[1] * scale + scale)
    can.create_oval(player[0] * scale, player[1] * scale, player[0] * scale + playerhw[1]+10, player[1] * scale + playerhw[0], fill = 'GREEN')
canprint(map, player, playerhw)

def destroy(*args):
    root.destroy()


def restart(widget=None, *args):
    global player
    #for i in range (0, 80) :
    player = [2, 5]
    canprint(map, player, playerhw)
    if widget != None:
        widget.destroy()
    movedown()


def win():
    global player
    #for i in range (0, 80) :
    player = [2, 5]
    window = Toplevel(root)
    window.focus_force()
    window.title('You win!')

    frame = ttk.Frame(window, padding="3 3 12 12")
    frame.grid(column=0, row=0, sticky=(N, W, E, S))
    frame.columnconfigure(0, weight=1)
    frame.rowconfigure(0, weight=1)

    ttk.Label(frame, text='Well done! You Win!!').grid(column=1, row=1, sticky=(N, S, E, W))
    ttk.Button(frame, text='Restart', command=lambda: restart(window)).grid(column=1, row=2, sticky=(N, S, E, W))
    ttk.Button(frame, text='Exit', command=destroy).grid(column=1, row=3, sticky=(N, S, E, W))

    window.bind('<Return>', lambda x: restart(window))
    window.bind('<q>', destroy)

    for child in window.winfo_children():
        child.grid_configure(padx=5, pady=5)

def death():
    global player
    #for i in range (0, 80) :
    player = [2, 5]
    window = Toplevel(root)
    window.focus_force()
    window.title('You lose :(')

    frame = ttk.Frame(window, padding="3 3 12 12")
    frame.grid(column=0, row=0, sticky=(N, W, E, S))
    frame.columnconfigure(0, weight=1)
    frame.rowconfigure(0, weight=1)


    ttk.Label(frame, text='You died.').grid(column=1, row=1, sticky=(N, S, E, W))
    ttk.Button(frame, text='Restart (Enter)', command=lambda: restart(window)).grid(column=1, row=2, sticky=(N, S, E, W))
    ttk.Button(frame, text='Quiz (q)', command=destroy).grid(column=1, row=3, sticky=(N, S, E, W))

    window.bind('<Return>', lambda x: restart(window))
    window.bind('<q>', destroy)

    for child in window.winfo_children():
        child.grid_configure(padx=20, pady=5)

def moveleft(*args) :
    player[0] -= Collision.speed
    check = Collision.collision(player, playerhw, map)
    if check == False :
        player[0] += Collision.speed
    if check == 'A':
        win()
    if check == 'S':
        death()
    canprint(map, player, playerhw)

def moveright(*args) :
    player[0] += Collision.speed
    check = Collision.collision(player, playerhw, map)
    if check == False :
        player[0] -= Collision.speed
    if check == 'A':
        win()

    if check == 'S':
        death()
    canprint(map, player, playerhw)

def moveup(*args) :
    wpressed = True
    player[1] -= Collision.speed
    check = Collision.collision(player, playerhw, map)
    if check == False :
        player[1] += Collision.speed
    if check == 'A':
        win()
    if check == 'S':
        death()
    canprint(map, player, playerhw)
    
def movedown(*args) :
    while Collision.collision([player[0], player[1] + 0.5], playerhw, map) == True :
        player[1] += Collision.speed
        canprint(map, player, playerhw)
        can.update()
    else:

        check = Collision.collision([player[0], player[1] + 0.5], playerhw, map)
        if check == 'A':
            win()
        if check == 'S':
            death()
    #player[1] -= 1


movedown()
root.bind("<a>",moveleft)
root.bind("<d>",moveright)
root.bind("<w>",moveup)
root.bind("<KeyRelease-w>",movedown)
root.bind("<KeyRelease-d>",movedown)
root.bind("<KeyRelease-a>",movedown)
root.bind_all('<r>', lambda x: restart())
root.bind_all('<Escape>', destroy)

root.mainloop()
