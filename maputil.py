#!/usr/bin/python3
# -*- coding:utf-8 -*-

# Everything to do with the map

def getmap():
    maparray = []
    with open('map.txt', 'r') as map:
        temp = []
        for line in map:
            for ltr in line:
                if ltr != '\n':
                    temp.append(ltr)

            maparray.append(temp)
            temp = []

    return maparray

map = getmap()



def getcharacterpos(map):
    for y, row in enumerate(map):
        for x, ltr in enumerate(row):
            if ltr == 'P':
                return (x, y)

    else:
        return False



